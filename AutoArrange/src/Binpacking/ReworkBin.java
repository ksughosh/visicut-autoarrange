/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Binpacking;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import Canvas.*;
import Utilities.*;

/**
 *
 * @author sughoshkumar
 */
public class ReworkBin {
    private static final String PIECES_PATH = "_your_original_points_file_path_";
    private static final String BINS_PATH = "_your_output_points_file_path_";
    private static final String OUTPUT = "_your_desired_output_directory_path_";

    private static Dimension binDimension;
    private static Dimension viewPortDimension;

    private static class BinLine {
        private int id;
        private double rotation;
        private CanvasPointDouble position;

        public BinLine(int id, double rotation, CanvasPointDouble position) {
            super();
            this.id = id;
            this.rotation = rotation;
            this.position = position;
        }
    }

    private static class BinFile {
        private String name;
        private int nLines;
        private ArrayList<BinLine> lines;

        public BinFile(int nLines) {
            super();
            this.nLines = nLines;
            lines = new ArrayList<ReworkBin.BinLine>();
        }

        public void addLine(BinLine line) {
            this.lines.add(line);
        }

        @Override
        public String toString() {
            System.out.println(nLines);
            for (BinLine line : lines) {
                System.out.println(line.id + " " + line.rotation + " " + line.position.toString());
            }
            return "nothing";

        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("Begin..............................................");
        CanvasArea[] pieces = loadPieces(PIECES_PATH);
        Map<Integer, CanvasArea> piecesMap = new HashMap<Integer, CanvasArea>();
        for (CanvasArea piece : pieces) {
            piecesMap.put(piece.getID(), piece);
        }
        BinFile[] bins = loadBinResultFiles(BINS_PATH);
        // recreate bins images
        int b = 0;
        for (BinFile bin : bins) {
            b++;
            ArrayList<CanvasArea> piecesInThisBin = new ArrayList<CanvasArea>();
            for (BinLine line : bin.lines) {
                CanvasArea piece = piecesMap.get(line.id);
                if (piece == null)
                    throw new Exception("Piece not found in the file");
                piece.rotate(line.rotation);
                piece.placeInPosition(line.position.x, line.position.y);
                piecesInThisBin.add(piece);
                piecesMap.remove(line.id);
            }
            Utils.drawCanvasAreasToFile(piecesInThisBin, viewPortDimension, binDimension, (OUTPUT + bin.name));
        }
        if (!piecesMap.isEmpty()) {
            throw new Exception("Some pieces where not present in the bin output files");
        }
        System.out.println("...................................................End");
    }

    private static CanvasArea[] loadPieces(String pathToFile) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(pathToFile));
        binDimension = new Dimension(sc.nextInt(), sc.nextInt());
        double x1 = binDimension.getWidth();
        double y1 = binDimension.getHeight();
        if (x1 > y1) {
            viewPortDimension = new Dimension(1500, (int) (1500 / (x1 / y1)));
        } else {
            viewPortDimension = new Dimension((int) (1500 / (y1 / x1)), 1500);
        }
        int N = sc.nextInt();
        sc.nextLine();
        CanvasArea[] pieces = new CanvasArea[N];
        int n = 0;
        while (n < N) {
            String s = sc.nextLine();
            String[] src = s.split("\\s+");
            if (src[0].equalsIgnoreCase("@")) {
                // hole piece
                if (n <= 0)
                    return null;

                CanvasPointDouble[] points = new CanvasPointDouble[src.length - 1];
                for (int j = 1; j < src.length; j++) {
                    String[] point = src[j].split(",");
                    double x = Double.valueOf(point[0]);
                    double y = Double.valueOf(point[1]);
                    points[j - 1] = new CanvasPointDouble(x, y);
                }
                CanvasArea outer = pieces[n - 1];
                outer.placeInPosition(0, 0);
                CanvasArea inner = new CanvasArea(points, n);
                inner.placeInPosition(0, 0);
                CanvasArea area = new CanvasArea(outer, inner);
                area.placeInPosition(0, 0);
                pieces[n - 1] = area;
            } else {
                CanvasPointDouble[] points = new CanvasPointDouble[src.length];
                for (int j = 0; j < src.length; j++) {
                    String[] point = src[j].split(",");
                    double x = Double.valueOf(point[0]);
                    double y = Double.valueOf(point[1]);
                    points[j] = new CanvasPointDouble(x, y);
                }
                pieces[n] = new CanvasArea(points, n + 1);
                ++n;
            }
        }
        sc.close();
        return pieces;
    }

    private static BinFile[] loadBinResultFiles(String pathToDirectory) throws FileNotFoundException {
        File folder = new File(pathToDirectory);
        File[] listOfFiles = folder.listFiles();
        ArrayList<String> fileNames = new ArrayList<String>();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                fileNames.add(listOfFiles[i].getName());
            }
        }
        ArrayList<BinFile> binFiles = new ArrayList<BinFile>();
        for (String file : fileNames) {
            binFiles.add(readBinFile(pathToDirectory + file, file));
        }
        return binFiles.toArray(new BinFile[0]);
    }

    private static BinFile readBinFile(String filename, String name) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(filename));
        int N = sc.nextInt();
        sc.nextLine();
        BinFile binFile = new BinFile(N);
        binFile.name = name;
        int n = 0;
        while (n < N) {
            String s = sc.nextLine();
            String[] src = s.split("\\s+");
            int id = Integer.valueOf(src[0]);
            double rotation = Double.valueOf(src[1]);

            String[] point = src[2].split(",");
            double x = Double.valueOf(point[0]);
            double y = Double.valueOf(point[1]);
            CanvasPointDouble position = new CanvasPointDouble(x, y);

            BinLine line = new BinLine(id, rotation, position);

            binFile.addLine(line);
            ++n;
        }
        sc.close();
        return binFile;
    }
}
