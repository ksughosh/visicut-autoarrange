/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Binpacking;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import Canvas.*;
import Utilities.*;

/**
 *
 * @author sughoshkumar
 */
public class Bin {
    private Dimension dimension;
    private CanvasArea[] placedPieces;
    private int NPlaced;
    private ArrayList<Rectangle2D.Double> freeRectangles = new ArrayList<Rectangle2D.Double>();

    public Bin(Dimension d) {
        this.dimension = new Dimension(d.width, d.height);
        NPlaced = 0;
        freeRectangles.add(new Rectangle2D.Double(0, 0, d.getWidth(), d.getHeight()));
    }

    public CanvasArea[] getPlacedPieces() {
        return placedPieces;
    }

    public int getNPlaced() {
        return NPlaced;
    }

    public double getOccupiedArea() {
        double occ = 0;
        for (int i = 0; i < NPlaced; i++) {
            placedPieces[i].updateArea();
            occ += placedPieces[i].getArea();
        }
        return occ;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public double getEmptyArea() {
        double occ = 0;
        for (int i = 0; i < NPlaced; i++) {
            occ += placedPieces[i].getArea();
        }
        return dimension.getWidth() * dimension.getHeight() - occ;
    }

    public CanvasArea[] BBCompleteStrategy(CanvasArea[] toPlace) {
        int lastIndex = 0;
        CanvasArea[] stillNotPlaced = boundingBoxPacking(toPlace);
        boolean movement = moveAndReplace(lastIndex);
        while (movement) {
            lastIndex = this.placedPieces.length;
            stillNotPlaced = boundingBoxPacking(stillNotPlaced);
            movement = moveAndReplace(lastIndex);
        }
        return stillNotPlaced;
    }

    public CanvasArea[] boundingBoxPacking(CanvasArea[] pieces) {
        ArrayList<CanvasArea> placedPieces = new ArrayList<CanvasArea>();
        ArrayList<CanvasArea> notPlacedPieces = new ArrayList<CanvasArea>();

        Arrays.sort(pieces, CanvasArea.BY_AREA);

        CanvasArea total = new CanvasArea();
        if (this.placedPieces != null) {
            for (CanvasArea a : this.placedPieces) {
                total.add(a);
                placedPieces.add(a);
            }
        }

        for (int i = pieces.length - 1; i >= 0; i--) {
            int where = findWhereToPlace(pieces[i], freeRectangles);
            if (where != -1) {
                Rectangle2D.Double freeRect = freeRectangles.get(where);
                CanvasArea placed = new CanvasArea(pieces[i], pieces[i].getID());
                placed.placeInPosition(freeRect.getX(), freeRect.getMaxY());
                if (!placed.intersection(total)) {
                    Rectangle2D.Double pieceBB = placed.getBoundingBox2D();
                    splitScheme(freeRect, pieceBB, freeRectangles);
                    computeFreeRectangles(pieceBB, freeRectangles);
                    eliminateNonMaximal();
                    placedPieces.add(placed);
                    total.add(placed);
                } else {
                    notPlacedPieces.add(pieces[i]);
                }
            } else {
                notPlacedPieces.add(pieces[i]);
            }
        }
        this.placedPieces = placedPieces.toArray(new CanvasArea[0]);
        NPlaced = this.placedPieces.length;
        return notPlacedPieces.toArray(new CanvasArea[0]);
    }

    private int findWhereToPlace(CanvasArea piece, ArrayList<Rectangle2D.Double> freeRectangles) {
        boolean lastRotated = false;
        Rectangle2D pieceBB = piece.getBoundingBox2D();
        int res = -1;
        double min = Double.MAX_VALUE;
        for (int i = freeRectangles.size() - 1; i >= 0; i--) {
            Rectangle2D.Double freeRect = freeRectangles.get(i);
            if (Utils.fits(pieceBB, freeRect)) {
                double m = Math.min(freeRect.getWidth() - pieceBB.getWidth(), freeRect.getHeight() - pieceBB.getHeight());
                if (m < min) {
                    min = m;
                    res = i;
                    if (lastRotated) {
                        piece.rotate(90);
                        lastRotated = false;
                    }

                }
            }
            if (Utils.fitsRotated(pieceBB, freeRect)) {
                double m = Math.min(freeRect.getWidth() - pieceBB.getHeight(), freeRect.getHeight() - pieceBB.getWidth());
                if (m < min) {
                    min = m;
                    res = i;
                    if (!lastRotated) {
                        piece.rotate(90);
                        lastRotated = true;
                    }
                }
            }
        }
        return res;
    }

    private void splitScheme(Rectangle2D.Double usedFreeArea, Rectangle2D.Double justPlacedPieceBB, ArrayList<Rectangle2D.Double> freeRectangles) {
        freeRectangles.remove(usedFreeArea);
        double width = usedFreeArea.getWidth();
        double height = justPlacedPieceBB.getY() - usedFreeArea.getY();
        if (height > 0) {
            Rectangle2D.Double upRect = new Rectangle2D.Double(usedFreeArea.getX(), usedFreeArea.getY(), width, height);
            freeRectangles.add(upRect);
        }

        width = usedFreeArea.getMaxX() - justPlacedPieceBB.getMaxX();
        height = usedFreeArea.getHeight();
        if (width > 0) {
            Rectangle2D.Double rightRect = new Rectangle2D.Double(justPlacedPieceBB.getMaxX(), usedFreeArea.getY(), width, height);
            freeRectangles.add(rightRect);
        }
    }

    private void computeFreeRectangles(Rectangle2D.Double justPlacedPieceBB, ArrayList<Rectangle2D.Double> freeRectangles) {
        Rectangle2D.Double[] rects = freeRectangles.toArray(new Rectangle2D.Double[0]);
        for (Rectangle2D.Double freeR : rects) {
            if (freeR.intersects(justPlacedPieceBB)) {
                freeRectangles.remove(freeR);
                Rectangle2D rIntersection = freeR.createIntersection(justPlacedPieceBB);
                // top
                double widht = freeR.getWidth();
                double height = rIntersection.getY() - freeR.getY();
                if (height > 0) {
                    Rectangle2D.Double upR = new Rectangle2D.Double(freeR.getX(), freeR.getY(), widht, height);
                    freeRectangles.add(upR);
                }

                // left
                widht = rIntersection.getX() - freeR.getX();
                height = freeR.getHeight();
                if (widht > 0) {
                    Rectangle2D.Double leftR = new Rectangle2D.Double(freeR.getX(), freeR.getY(), widht, height);
                    freeRectangles.add(leftR);
                }

                // bottom
                widht = freeR.getWidth();
                height = freeR.getMaxY() - rIntersection.getMaxY();
                if (height > 0) {
                    Rectangle2D.Double bottomR = new Rectangle2D.Double(freeR.getX(), rIntersection.getMaxY(), widht, height);
                    freeRectangles.add(bottomR);
                }

                // right
                widht = freeR.getMaxX() - rIntersection.getMaxX();
                height = freeR.getHeight();
                if (widht > 0) {
                    Rectangle2D.Double rightR = new Rectangle2D.Double(rIntersection.getMaxX(), freeR.getY(), widht, height);
                    freeRectangles.add(rightR);
                }
            }
        }
    }

    private void eliminateNonMaximal() {
        Rectangle2D.Double[] freeRectArray = freeRectangles.toArray(new Rectangle2D.Double[0]);
        Arrays.sort(freeRectArray, RECTANGLE_AREA_COMPARATOR);
        freeRectangles.clear();
        for (int i = 0; i < freeRectArray.length; i++) {
            boolean contained = false;
            for (int j = freeRectArray.length - 1; j >= i; j--) {
                if (j != i && freeRectArray[j].contains(freeRectArray[i])) {
                    contained = true;
                    break;
                }
            }
            if (!contained)
                freeRectangles.add(freeRectArray[i]);
        }
    }

    private boolean moveAndReplace(int indexLimit) {
        boolean movement = false;
        CanvasArea total = new CanvasArea();
        for (CanvasArea area : this.placedPieces)
            total.add(area);
        int currentIndex = this.placedPieces.length - 1;
        while (currentIndex >= indexLimit) {
            CanvasArea currentArea = this.placedPieces[currentIndex];
            total.subtract(currentArea);
            for (int i = 0; i < currentIndex; i++) {
                CanvasArea container = this.placedPieces[i];
                if (container.getFreeArea() > currentArea.getArea()) {
                    CanvasArea auxArea = new CanvasArea(currentArea, currentArea.getID());
                    Rectangle2D.Double contBB = container.getBoundingBox2D();
                    auxArea.placeInPosition(contBB.getX(), contBB.getY());
                    auxArea = sweep(container, auxArea, total);
                    if (auxArea != null) {
                        freeRectangles.add(currentArea.getBoundingBox2D());
                        compress(total, auxArea, new Rectangle(dimension), new CanvasVector(-1, 1));
                        placedPieces[currentIndex] = auxArea;
                        computeFreeRectangles(auxArea.getBoundingBox2D(), freeRectangles);
                        eliminateNonMaximal();
                        movement = true;
                        break;
                    } else {
                        auxArea = new CanvasArea(currentArea, currentArea.getID());
                        auxArea.rotate(90);
                        auxArea.placeInPosition(contBB.getX(), contBB.getY());
                        auxArea = sweep(container, auxArea, total);
                        if (auxArea != null) {
                            freeRectangles.add(currentArea.getBoundingBox2D());
                            compress(total, auxArea, new Rectangle(dimension), new CanvasVector(-1, 1));
                            placedPieces[currentIndex] = auxArea;
                            computeFreeRectangles(auxArea.getBoundingBox2D(), freeRectangles);
                            eliminateNonMaximal();
                            movement = true;
                            break;
                        }
                    }
                }
            }
            total.add(placedPieces[currentIndex]);
            currentIndex--;
        }
        return movement;
    }

    private CanvasArea sweep(CanvasArea container, CanvasArea inside, CanvasArea collisionArea) {
        if (!inside.intersection(collisionArea))
            if (inside.isToLeft(container.getBoundingBox()))
                return inside;
        CanvasArea lastValidPosition = null;
        double Xcontainer = container.getBoundingBox().getX();
        double dx = inside.getBoundingBox().getWidth() / Constants.DX_SWEEP_FACTOR;
        double dy = inside.getBoundingBox().getHeight() / Constants.DY_SWEEP_FACTOR;
        Rectangle2D.Double containerBB = container.getBoundingBox2D();
        CanvasArea originalArea = new CanvasArea(inside, inside.getID());
        while (true) {
            boolean check = false;
            inside.move(new CanvasVector(dx, 0));
            if (!inside.intersection(collisionArea) && inside.isToLeft(new Rectangle(dimension)))
                check = true;
            Rectangle2D.Double insideBB = inside.getBoundingBox2D();
            if (!containerBB.contains(insideBB.getX(), insideBB.getY())) {
                if (!inside.intersection(collisionArea) && inside.isInside(new Rectangle(dimension))) {
                    check = true;
                    lastValidPosition = new CanvasArea(inside, inside.getID());
                    break;

                }
                inside.move(new CanvasVector(-(inside.getBoundingBox().getX() - Xcontainer), 0));
                inside.move(new CanvasVector(0, dy));
                if (!inside.isAbove(container.getBoundingBox())) {
                    check = false;
                    break;
                }
                if (inside.intersection(collisionArea))
                    check = false;
                else
                    check = true;
            }
            if (check) {
                if (!inside.intersection(collisionArea) && inside.isInside(new Rectangle(dimension))) {
                    lastValidPosition = new CanvasArea(inside, inside.getID());
                    break;
                }
            }
        }
        if (lastValidPosition != null) {
            CanvasArea containerBBArea = new CanvasArea(containerBB, 0);
            if (!lastValidPosition.intersection(containerBBArea)) {
                CanvasPointDouble initialPos = new CanvasPointDouble(originalArea.getBoundingBox2D().getX(), originalArea.getBoundingBox2D().getY());
                CanvasPointDouble finalPos = new CanvasPointDouble(lastValidPosition.getBoundingBox2D().getX(), lastValidPosition.getBoundingBox2D().getY());
                CanvasPointDouble containerPos = new CanvasPointDouble(containerBB.getX(), containerBB.getY());
                if (containerPos.distance(finalPos) > containerPos.distance(initialPos)) {
                    return null;
                }
            }
        }
        return lastValidPosition;
    }

    private static final Comparator<Rectangle2D> RECTANGLE_AREA_COMPARATOR = new RectangleAreaComparator();

    private static class RectangleAreaComparator implements Comparator<Rectangle2D> {

        @Override
        public int compare(Rectangle2D arg0, Rectangle2D arg1) {
            double area0 = arg0.getWidth() * arg0.getHeight();
            double area1 = arg1.getWidth() * arg1.getHeight();
            if (area0 < area1)
                return -1;
            if (area1 < area0)
                return 1;
            return 0;
        }
    }

    public void compress() {
        if (this.NPlaced <= 0) {
            return;
        }
        CanvasArea total = new CanvasArea();
        for (CanvasArea a : this.placedPieces)
            total.add(a);
        boolean moved = true;
        Rectangle container = new Rectangle(0, 0, dimension.width, dimension.height);
        while (moved) {
            for (CanvasArea a : this.placedPieces) {
                total.subtract(a);
                moved = compress(total, a, container, new CanvasVector(-1, 1));
                total.add(a);
            }
        }
    }

    private boolean compress(CanvasArea collisionArea, CanvasArea compressArea, Rectangle container, CanvasVector vector) {
        int movement = 0;
        if (vector.getX() == 0 && vector.getY() == 0)
            return false;
        boolean moved = true;
        while (moved) {
            moved = false;
            if (vector.getY() != 0) {
                // y direction
                CanvasVector u = new CanvasVector(0, vector.getY());
                compressArea.move(u);
                movement++;
                if (compressArea.isInside(container) && !compressArea.intersection(collisionArea))
                    moved = true;
                else {
                    compressArea.move(u.inverse());
                    movement--;
                }
            }
            if (!moved) {
                if (vector.getX() != 0) {
                    // x direction
                    CanvasVector u = new CanvasVector(vector.getX(), 0);
                    compressArea.move(u);
                    movement++;
                    if (compressArea.isInside(container) && !compressArea.intersection(collisionArea))
                        moved = true;
                    else {
                        compressArea.move(u.inverse());
                        movement--;
                    }
                }
            }
        }
        if (movement > 0)
            return true;
        return false;
    }

    public CanvasArea[] dropPieces(CanvasArea[] notPlaced) {
        ArrayList<CanvasArea> noPlaced = new ArrayList<CanvasArea>();
        CanvasArea total = new CanvasArea();
        ArrayList<CanvasArea> placedPieces = new ArrayList<CanvasArea>();
        for (CanvasArea area : this.placedPieces) {
            total.add(area);
            placedPieces.add(area);
        }

        Rectangle container = new Rectangle(dimension);
        for (int i = 0; i < notPlaced.length; i++) {
            boolean placed = false;
            for (int angle : Constants.ROTATION_ANGLES) {
                CanvasArea tryArea = new CanvasArea(notPlaced[i], notPlaced[i].getID());
                tryArea.rotate(angle);
                tryArea = dive(tryArea, container, total, new CanvasVector(0, 1));
                if (tryArea != null) {
                    total.add(tryArea);
                    placedPieces.add(tryArea);
                    placed = true;
                    break;
                }
            }
            if (!placed) {
                noPlaced.add(notPlaced[i]);
            }
        }
        this.placedPieces = placedPieces.toArray(new CanvasArea[0]);
        this.NPlaced = this.placedPieces.length;
        return noPlaced.toArray(new CanvasArea[0]);
    }

    private CanvasArea dive(CanvasArea toDive, Rectangle container, CanvasArea collisionArea, CanvasVector vector) {
        Rectangle2D.Double toDiveBB = toDive.getBoundingBox2D();

        // only takes into account dimensions, not position
        if (!Utils.fits(toDiveBB, container)) {
            return null;
        }
        double dx = toDiveBB.getWidth() / Constants.DIVE_HORIZONTAL_DISPLACEMENT_FACTOR;
        double initialX = 0;
        while (true) {
            toDive.placeInPosition(initialX, 0);
            if (!toDive.intersection(collisionArea)) {
                compress(collisionArea, toDive, container, vector);
                return toDive;
            }
            initialX += dx;
            toDive.placeInPosition(initialX, 0);

            // verify that the pieces hasn't exceeded the container's boundaries
            toDiveBB = toDive.getBoundingBox2D();
            if ((initialX + toDiveBB.getWidth() > container.getMaxX()) || (toDiveBB.getHeight() > container.getMaxY())) {
                return null;
            }
        }

    }
}
