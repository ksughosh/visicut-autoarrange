/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Binpacking;
import Canvas.*;
import java.awt.Dimension;
import java.util.ArrayList;

/**
 *
 * @author sughoshkumar
 */
public class BinPacking {
    public static Bin[] BinPackingStrategy(CanvasArea[] pieces, Dimension binDimension, Dimension viewPortDimension) {
        System.out.println(".............Started computation of bin placements.............");
        ArrayList<Bin> bins = new ArrayList<Bin>();
        int nbin = 0;
        boolean stillToPlace = true;
        CanvasArea[] notPlaced = pieces;
        double t1 = System.currentTimeMillis();
        int count = 0;
        while (stillToPlace && count < 10) {
            stillToPlace = false;
            Bin bin = new Bin(binDimension);
            notPlaced = bin.BBCompleteStrategy(notPlaced);

            bin.compress();

            notPlaced = bin.dropPieces(notPlaced);

            System.out.println("Bin " + (++nbin) + " generated");
            bins.add(bin);
            if (notPlaced.length > 0)
                stillToPlace = true;
            count++;
        }
        double t2 = System.currentTimeMillis();
        System.out.println();
        System.out.println("Number of used bins: " + nbin);
        System.out.println("Computation time:" + ((t2 - t1) / 1000) / 60 + " minutes");
        System.out.println();
        return bins.toArray(new Bin[0]);
    }
}
