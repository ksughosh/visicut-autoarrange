/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Canvas;

import java.awt.geom.Point2D;

/**
 *
 * @author sughoshkumar
 */
public class CanvasPointDouble extends Point2D.Double implements Comparable<CanvasPointDouble> {

    public CanvasPointDouble(double x, double y) {
        super(x, y);
    }

    /**
     * Comparable method
     */
    @Override
    public int compareTo(CanvasPointDouble o) {
        if (this.x < o.x)
            return -1;
        if (this.x > o.x)
            return 1;
        if (this.y < o.y)
            return -1;
        if (this.y > o.y)
            return 1;
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        CanvasPointDouble pobj = (CanvasPointDouble) obj;
        return (this.x == pobj.x && this.y == pobj.y);
    }
    
}
