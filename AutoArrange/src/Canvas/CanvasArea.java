/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Canvas;

import Utilities.Utils;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author sughoshkumar
 */
public class CanvasArea extends Area {
    private double area;
    int ID;

    double rotation;

    public CanvasArea(Path2D path, int ID){
        super(path);
        computeArea();
        this.ID = ID;
        rotation = 0;
    }

    public CanvasArea(Rectangle rectangle, int ID){
        super(rectangle);
        this.area = rectangle.getHeight() * rectangle.getWidth();
        this.ID = ID;
        rotation = 0;
    }

    public CanvasArea(CanvasArea canvasArea, int ID){
        super(canvasArea);
        this.area = canvasArea.area;
        this.ID = ID;
        rotation = 0;
    }

    public CanvasArea (Rectangle2D.Double rectangle, int ID){
        super(rectangle);
        this.area = rectangle.getHeight() * rectangle.getWidth();
        this.ID = ID;
        rotation = 0;
    }

    //Empty area
    public CanvasArea(int ID){
        super();
        this.ID = ID;
        rotation = 0;
    }

    //Empty Constructor
    public CanvasArea(){
        super();
        this.area = 0;
        rotation = 0;
    }

    //Area with a hole
    public CanvasArea(CanvasArea out, CanvasArea in){
        super(canvasAreaHolesConstructor(out,in));
        this.ID = ID;
        computeArea();
        rotation = 0;
    }

    //Area with a set of points
    public CanvasArea(CanvasPointDouble[] points, int ID){
        super(Utils.createShape(points));
        this.ID = ID;
        computeArea();
        rotation = 0;
    }

    //Holes constructor
    public static CanvasArea canvasAreaHolesConstructor(CanvasArea out, CanvasArea in){
        CanvasArea area = new CanvasArea(out, out.getID());
        area.subtract(in);
        return area;
    }

    // compute area
    private double computeArea() {
        double area = 0;
        CanvasPointDouble[] points = getPoints();
        for (int i = 0; i < points.length; i++) {
            int j = (i + 1) % points.length;
            area += points[i].x * points[j].y;
            area -= points[i].y * points[j].x;
        }

        area /= 2;
        this.area = area < 0 ? -area : area;
        return this.area;
    }

    public Rectangle getBoundingBox(){
        return(Rectangle) this.getBounds();
    }

    public Rectangle2D.Double getBoundingBox2D(){
        return (Rectangle2D.Double) this.getBounds2D();
    }

    public double getFreeArea(){
        updateArea();
        Rectangle boundingBox = getBoundingBox();
        return (boundingBox.getWidth() * boundingBox.getHeight()-this.area);
    }

    public CanvasPointDouble[] getPoints(){
        ArrayList<CanvasPointDouble> points = new ArrayList<CanvasPointDouble>();
        for (PathIterator pi = this.getPathIterator(null); !pi.isDone();pi.next()){
            double[] coord = new double[6];
            int type = pi.currentSegment(coord);
            if (type != PathIterator.SEG_CLOSE){
                CanvasPointDouble ps = new CanvasPointDouble(coord[0], coord[1]);
                points.add(ps);
            }
        }
        return (points.toArray(new CanvasPointDouble[0]));
    }

    public double getArea(){
        return area;
    }

    public void updateArea(){
        computeArea();
    }

    public int getID(){
        return ID;
    }

    public void drawInViewPort(Dimension binDimension, Dimension viewPortDimension, Graphics g){
        double xFactor = viewPortDimension.getWidth() / binDimension.getWidth();
        double yFactor = viewPortDimension.getHeight() / binDimension.getHeight();
        AffineTransform transform = new AffineTransform();
        transform.scale(xFactor,yFactor);
        transform.translate(11,11);
        Area newArea = this.createTransformedArea(transform);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        g2d.setStroke(new BasicStroke(2));
        g2d.draw(newArea);
        g2d.setColor(Color.LIGHT_GRAY);
        g2d.fill(newArea);
    }

    @Override
    public boolean equals(Object obj){
        CanvasArea other = (CanvasArea) obj;
        return this.ID == other.ID;
    }

    public void move(CanvasVector vector){
        AffineTransform at = new AffineTransform();
        at.translate(vector.getX(), vector.getY());
        this.transform(at);
    }

    public void placeInPosition(double x, double y){
        Rectangle bb = getBoundingBox();
        AffineTransform transform = new AffineTransform();
        double thisX = bb.getX();
        double dx = Math.abs(thisX-x);
        if( thisX <= x){
            thisX = dx;
        }
        else {
            thisX = -dx;
        }

        double thisY = bb.getY();
        double dy = Math.abs(thisY-y);
        if( thisY <= y){
            thisY = dy;
        }
        else {
            thisY = -dy;
        }
        transform.translate(thisX, thisY);
        this.transform(transform);
    }

    public void rotate(double degrees){
        this.rotation += degrees;
        if(this.rotation >= 360)
            this.rotation -=360;
        AffineTransform transform = new AffineTransform();
        Rectangle rectangle = getBoundingBox();
        transform.rotate(Math.toRadians(degrees), rectangle.getX() + rectangle.getWidth()/2, rectangle.getY() + rectangle.getHeight()/2);
        this.transform(transform);
    }

    public boolean intersection(CanvasArea other){
        CanvasArea intersectionArea = new CanvasArea(this, this.ID);
        intersectionArea.intersect(other);
        return !intersectionArea.isEmpty();
    }

    public boolean isInside(Rectangle rect){
        Rectangle bb = getBoundingBox();

        double leftX = bb.getX();
        double leftY = bb.getY();

        if(leftX < rect.getX())
            return false;
        if(leftY < bb.getY())
            return false;

        double rightX = bb.getX() + bb.getWidth();
        double rightY = bb.getY() + bb.getHeight();

        if (rightX > (rect.getX() + rect.getWidth()))
            return false;
        if (rightY > (rect.getY() + rect.getHeight()))
            return false;
        return true;
    }

    public boolean isAbove(Rectangle rectangle){
        Rectangle boundingBox = getBoundingBox();
        double leftY = boundingBox.getY() + this.getBoundingBox().getHeight();
        if (leftY >= (rectangle.getY() + rectangle.getHeight()))
            return false;

        return true;
    }

    public boolean isToLeft(Rectangle rectangle) {
        Rectangle boundingBox = getBoundingBox();
        if (boundingBox.getMinX() > rectangle.getMaxX())
            return false;

        return true;
    }

    public double getRotation(){
        return rotation;
    }

    public static final Comparator<CanvasArea> BY_AREA = new ByArea();

    public static final Comparator<CanvasArea> BY_BOUNDING_BOX_AREA = new ByBoundingBoxArea();

    private static class ByArea implements Comparator<CanvasArea>{
        @Override
        public int compare(CanvasArea c1, CanvasArea c2){
            if ( c1.area > c2.area )
                return 1;
            if( c1.area < c2.area )
                return -1;
            return 0;
        }
    }

    private static class ByBoundingBoxArea implements Comparator<CanvasArea>{
        @Override
        public int compare(CanvasArea c1, CanvasArea c2){
            Rectangle c1bb = c1.getBoundingBox();
            Rectangle c2bb = c2.getBoundingBox();
            double bb1 = c1bb.getWidth() * c1bb.getHeight();
            double bb2 = c2bb.getWidth() * c2bb.getHeight();

            if( bb1 < bb2 )
                return -1;
            if( bb1 > bb2 )
                return 1;
            return 0;
        }
    }
}
