/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Utilities;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import javax.imageio.ImageIO;

import Canvas.*;

/**
 *
 * @author sughoshkumar
 */
public class Utils {
     /**
     * Creates a Path2D geometric object from a set of points. Used for creating
     * MArea objects. Points for the path need to be given in counter clockwise
     * order.
     *
     * @param points
     *            set of points
     * @return Path2D object.
     */
    public static Path2D createShape(CanvasPointDouble[] points) {
        Path2D path = new Path2D.Double();

        path.moveTo(points[0].getX(), points[0].getY());
        for (int i = 1; i < points.length; ++i) {
            path.lineTo(points[i].getX(), points[i].getY());
        }
        path.closePath();
        return path;
    }


    public static boolean fits(Rectangle2D o1, Rectangle2D o2) {
        return (o1.getHeight() <= o2.getHeight() && o1.getWidth() <= o2.getWidth());
    }


    public static boolean fitsRotated(Rectangle2D o1, Rectangle2D o2) {
        return (o1.getHeight() <= o2.getWidth() && o1.getWidth() <= o2.getHeight());
    }


    public static boolean equalDimension(Rectangle o1, Rectangle o2) {
        return (o1.getWidth() == o2.getWidth() && o1.getHeight() == o2.getHeight());
    }


    public static void drawCanvasAreasToFile(ArrayList<CanvasArea> pieces, Dimension viewPortDimension, Dimension binDimension, String name) throws IOException {
        BufferedImage img = new BufferedImage(viewPortDimension.width + 20, viewPortDimension.height + 20, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = img.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(Color.WHITE);
        for (int i = 0; i < pieces.size(); i++) {
            pieces.get(i).drawInViewPort(binDimension, viewPortDimension, g2d);
        }
        File outputFile = new File(name + ".png");
        img = flipAroundX(img);
        ImageIO.write(img, "png", outputFile);
    }

    /**
     * Flips the image horizontally
     *
     * @param image
     *            image to flip
     * @return Modified image
     */
    public static BufferedImage flipAroundX(BufferedImage image) {
        // Flip the image vertically
        AffineTransform tx = AffineTransform.getScaleInstance(1, -1);
        tx.translate(0, -image.getHeight(null));
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        image = op.filter(image, null);
        return image;
    }

    public CanvasArea preprocessing(CanvasArea piece, Rectangle2D.Double container) throws Exception {
        Rectangle2D.Double bbox = piece.getBoundingBox2D();
        if (bbox.getWidth() > container.getWidth()) {
            piece.rotate(90);
            bbox = piece.getBoundingBox2D();
            if (bbox.getWidth() > container.getWidth() || bbox.getHeight() > container.getHeight())
                throw new Exception("The piece dimensions seem to be bigger than the container's");
            return piece;
        }
        if (bbox.getHeight() > container.getHeight()) {
            piece.rotate(90);
            bbox = piece.getBoundingBox2D();
            if (bbox.getWidth() > container.getWidth() || bbox.getHeight() > container.getHeight())
                throw new Exception("The piece dimensions seem to be bigger than the container's");
            return piece;
        }
        return piece;
    }

    /**
     * Load pieces file
     *
     * @param fileName
     * @return @Object[] that contains the specified bin dimension, the
     *         calculated viewport dimension and the pieces read from file:
     *         position 0 - (Dimension)binDimension position 1 -
     *         (Dimension)viewPortDimension position 2 - (MArea[])pieces
     * @throws IOException
     */
    public static Object[] loadPieces(String fileName, Dimension vPDimension) throws IOException {
        Scanner sc = new Scanner(new File(fileName));
        Dimension binDimension = new Dimension(sc.nextInt(), sc.nextInt());
        double x1 = binDimension.getWidth();
        double y1 = binDimension.getHeight();
        Dimension viewPortDimension;
        viewPortDimension = vPDimension;
        int N = sc.nextInt();
        sc.nextLine();
        CanvasArea[] pieces = new CanvasArea[N];
        int n = 0;
        while (n < N) {
            String s = sc.nextLine();
            String[] src = s.split("\\s+");
            if (src[0].equalsIgnoreCase("@")) {
                // hole piece
                if (n <= 0) {
                    sc.close();
                    return null;
                }
                CanvasPointDouble[] points = new CanvasPointDouble[src.length - 1];
                for (int j = 1; j < src.length; j++) {
                    String[] point = src[j].split(",");
                    double x = Double.valueOf(point[0]);
                    double y = Double.valueOf(point[1]);
                    points[j - 1] = new CanvasPointDouble(x, y);
                }
                CanvasArea outer = pieces[n - 1];
                CanvasArea inner = new CanvasArea(points, n);
                CanvasArea area = new CanvasArea(outer, inner);
                area.placeInPosition(0, 0);
                pieces[n - 1] = area;
                // TODO process repetitions in the holes processing section
            } else {
                ArrayList<CanvasPointDouble> pointsArrayList = new ArrayList<CanvasPointDouble>();
                HashSet<CanvasPointDouble> set = new HashSet<CanvasPointDouble>();
                for (int j = 0; j < src.length; j++) {
                    String[] point = src[j].split(",");
                    double x = Double.valueOf(point[0]);
                    double y = Double.valueOf(point[1]);
                    CanvasPointDouble thisPoint = new CanvasPointDouble(x, y);
                    if (!set.contains(thisPoint)) {
                        pointsArrayList.add(thisPoint);
                        set.add(thisPoint);
                    }
                }
                pieces[n] = new CanvasArea(pointsArrayList.toArray(new CanvasPointDouble[0]), n + 1);
                ++n;
            }
        }
        sc.close();
        Object[] result = { binDimension, viewPortDimension, pieces };
        return result;
    }
}
