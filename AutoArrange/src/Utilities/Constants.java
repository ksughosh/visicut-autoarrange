/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Utilities;

/**
 *
 * @author sughoshkumar
 */
public class Constants {
    public static final int DIVE_HORIZONTAL_DISPLACEMENT_FACTOR = 3;
    public static final int DX_SWEEP_FACTOR = 10;
    public static final int DY_SWEEP_FACTOR = 2;
    public static final int[] ROTATION_ANGLES = { 0, 90 };
}
